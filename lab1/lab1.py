import time
import RPi.GPIO as GPIO

SWITCH_PIN = [11] #定義switch腳位為11號GPIO pin
LED_PIN = [32, 36, 38, 40] #定義LED腳位為32、36、38、40號GPIO pin，上述編號依序由左到右對應LED燈
GPIO.setmode(GPIO.BOARD) #定義模式

GPIO.setup(SWITCH_PIN, GPIO.IN) #告訴RPi，上述腳位們是input or output
GPIO.setup(LED_PIN, GPIO.OUT)

a=0 #a是計數器，預設0

try:
	while True:
		if GPIO.input(SWITCH_PIN[0]) == GPIO.HIGH:
			a+=1 #只要switch作動，計數器就加一
			
		if a<16: #超過15的話4位元2進位塞不下，所以多一個if
			b = '{:04b}'.format(a) #把十進位數字換成4位元2進位的字串
	
		else:	
			a=0 #超過15的話洗回0
		
		for i in range(4): #查看目前計數器給的二位元表示字串，查看第i+1位數是1還是0，對應給i+1號燈作動，1就亮、0就暗
			if b[i]=="1":
				GPIO.output(LED_PIN[i], GPIO.HIGH)
			else:
				GPIO.output(LED_PIN[i], GPIO.LOW)
    
		time.sleep(0.5) #等0.5秒
        
except KeyboardInterrupt: #鍵盤有作動就停止
    print("kb")
finally:
    GPIO.cleanup() #釋放腳位